/**
 * Created by vitaliy on 07.07.17.
 */

$(document).ready(function() {
  indexPageScrolling();
  addMobileMenuListener();
  addSubmitFeedbackListener();
  serviceClickListener('#services-modal', '.services-item');
});

function indexPageScrolling() {
  launchPageScrolling('#fullpage',
    ['getStarted', 'welcome', 'providers', 'paymentCenter', 'servicesOffer', 'feedback'],
    ['', 'Welcome', 'Service providers', 'Payment center', 'Services we offer', 'Contact us']);
}

function addMobileMenuListener() {
  $('.mobile-menu-icon').on('click', function() {
    $('.mobile-menu').toggleClass('opened');
    $(this).toggleClass('opened');
  });
}

function addSubmitFeedbackListener() {
  $('#feedback-form').on('submit', sendFeedbackForm);
}