/**
 * Created by vitaliy on 10.07.17.
 */
function launchPageScrolling(selector, anchors, navigationTooltips) {

  $(selector).fullpage({
    anchors: anchors, //['getStarted', 'welcome', 'providers', 'paymentCenter', 'servicesOffer', 'feedback'],
    navigation: true,
    navigationPosition: 'right',
    navigationTooltips: navigationTooltips, //['', 'Welcome', 'Service providers', 'Payment center', 'Services we offer', 'Contact us'],
    scrollingSpeed: 1200,
    easing: 'easeOutQuart',
    easingcss3: 'ease',
    showActiveTooltip : true
  });
}

function addMobileMenuListener() {
  $('.mobile-menu-icon').on('click', function() {
    $('.mobile-menu').toggleClass('opened');
    $(this).toggleClass('opened');
  });
}

function serviceClickListener(selectorModal, selectorListener, parentSelector, cb) {
  $(selectorListener).on('click', function(e) {
    var contentTarget = parentSelector ? $(e.currentTarget).parents(parentSelector)[0] :  $(e.currentTarget);
    var content = $(contentTarget).html();

    $(selectorModal).html(content).modal();

    if (cb && typeof cb === 'function') cb(e);
  })
}

function sendFeedbackForm(event) {
  debugger;
  var targetForm = $(event.currentTarget);
  var contact_name = $(targetForm).find("input[name='contact_name']")[0];
  var name = $(targetForm).find("input[name='name']")[0];
  var email_from = $(targetForm).find("input[name='email_from']")[0];
  var description = $(targetForm).find("textarea[name='description']")[0];

  var allFieldsValid = true;

  if (!validators.validateSimpleText(contact_name.value)) {
    allFieldsValid = false;
    $(contact_name).find('+ span.error').text('Name is not valid');
  } else {
    $(contact_name).find(' + span.error').text('');
  }

  if (!validators.validateSimpleText(name.value)) {
    allFieldsValid = false;
    $(name).find(' + span.error').text('Subject is not valid');
  }else {
    $(name).find(' + span.error').text('');
  }

  if (!validators.validateSimpleText(description.value)) {
    allFieldsValid = false;
    $(description).find(' + span.error').text('Description is not valid');
  }else {
    $(description).find(' + span.error').text('');
  }

  if (!validators.validateEmail(email_from.value)) {
    allFieldsValid = false;
    $(email_from).find(' + span.error').text('Email is not valid');
  }else {
    $(email_from).find(' + span.error').text('');
  }

  if (!allFieldsValid) event.preventDefault();
}

var validators = {
  validateSimpleText : function (val) {
    if (!val || val.length < 2) return false;

    return true;
  },
  validateEmail : function (val) {
    if (!val || !(/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(val))) return false;
  }
};