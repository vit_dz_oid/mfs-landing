/**
 * Created by vitaliy on 10.07.17.
 */
$(document).ready(function() {
  ourTeamPageScrolling();
  addMobileMenuListener();
  serviceClickListener('#main-modal', '.founder__img', '.founder__item');
  addSubmitContactUsListener()
});

function ourTeamPageScrolling() {
  launchPageScrolling('#fullpage',
    ['ourTeam',''],
    ['']);
}

function addSubmitContactUsListener() {
  $('#contact-us-form').on('submit', sendFeedbackForm);
}
