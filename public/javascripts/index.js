/**
 * Created by vitaliy on 07.07.17.
 */

$(document).ready(function() {
  indexPageScrolling();
  addMobileMenuListener();
  addSubmitFeedbackListener();
  serviceClickListener('#services-modal', '.services-item');
});

function indexPageScrolling() {
  launchPageScrolling('#fullpage',
      ['getStarted', 'welcome', 'servicesOffer', 'providers', 'feedback'],
    ['', 'Welcome', 'Services we offer', 'Service providers', 'Contact us']);
	/*
	    ['getStarted', 'welcome', 'servicesOffer', 'paymentCenter', 'providers', 'feedback'],
    ['', 'Welcome', 'Services we offer', 'Payment center', 'Service providers', 'Contact us']);
	*/
}

function addMobileMenuListener() {
  $('.mobile-menu-icon').on('click', function() {
    $('.mobile-menu').toggleClass('opened');
    $(this).toggleClass('opened');
  });
}

function addSubmitFeedbackListener() {
  $('#feedback-form').on('submit', sendFeedbackForm);
}