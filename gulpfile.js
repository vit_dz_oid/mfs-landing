/**
 * Created by vitaliy on 03.07.17.
 */
var gulp = require('gulp');
var del = require('del');

var paths = {
    srcFront : 'public/**/*',
    htmls : 'views/*.html'
};

// Not all tasks need to use streams
// A gulpfile is just another node program and you can use any package available on npm
gulp.task('clean', function() {
    // You can use multiple globbing patterns as you would with `gulp.src`
    return del(['production'], { force : true });
});

// Copy all needed files
gulp.task('front', ['clean'], function() {
    return gulp.src(paths.srcFront)
        .pipe(gulp.dest('production'));
});

gulp.task('html', ['clean'], function() {
  return gulp.src(paths.htmls)
    .pipe(gulp.dest('production'));
});



// The default task (called when you run `gulp` from cli)
gulp.task('default', ['front', 'html']);